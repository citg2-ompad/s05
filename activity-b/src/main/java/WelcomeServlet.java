import java.io.IOException;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/welcome")
public class WelcomeServlet extends HttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		
		String firstname = req.getParameter("firstname");
		String lastname = req.getParameter("lastname");
		String typeOfWork = req.getParameter("type_of_work");

		HttpSession session = req.getSession();
		session.setAttribute("firstname", firstname);
		session.setAttribute("lastname", lastname);
		
		session.setAttribute("typeOfWork", typeOfWork);
		
		res.sendRedirect("confirmation.jsp");

		
	}
}
